import React, { Component } from "react";

import { Route, Switch } from "react-router-dom";

import Header from "../../containers/header/Header";
import Footer from "../../containers/footer/Footer";

import Main from "../../containers/Main/Main";
import Products from "../../containers/Products/Products";
import Services from "../../containers/Services/Services";
import Works from "../../containers/Works/Works";
import Contact from "../../containers/Contact/Contact";
import Reviews from "../../containers/Reviews/Reviews";
import ScrollToView from "../../components/ScrollToView";

class PublicLayout extends Component {
  render() {
    return (
      <div>
        <Header />
        <ScrollToView>
          <Switch>
            <Route path="/products" component={Products} />
            <Route path="/works" component={Works} />
            <Route path="/services" component={Services} />
            <Route path="/reviews" component={Reviews}/>
            <Route path="/contact" component={Contact} />
            <Route path="/" component={Main} />
          </Switch>
        </ScrollToView>
        <Footer />
      </div>
    );
  }
}

export default PublicLayout;
