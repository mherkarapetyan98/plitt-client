import React, { Component } from "react";
import "./ZoomImage.css";
import ServicesSlider from "../sliders/servicesSlider/ServicesSlider";
import "react-responsive-carousel/lib/styles/carousel.min.css";

class ZoomImage extends Component {
  render() {
    const cls = ["ZoomImage"];

    if (!this.props.isZoomed) {
      cls.push("close");
    }
    //  onClick={this.props.closeModal} for zooming just image without slider
    return (
      <div className={cls.join(" ")}>
        {/* <img src={this.props.imageSrc} alt={this.props.imageAlt} /> */}
        <div className="slider-image">
          <i className="far fa-times-circle" style={{cursor: 'pointer', color: 'rgba(255, 255, 255, 0.2)', fontSize: '35px', position: 'absolute', top: '1%',right: '15px'}} onClick={this.props.closeModal}/>
          <ServicesSlider selectedImage={this.props.selectedImage} moreImages={this.props.moreImages} />
        </div>
      </div>
    );
  }
}

export default ZoomImage;
