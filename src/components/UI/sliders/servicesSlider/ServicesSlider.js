import React, { Component } from "react";
import Slider from "react-slick";
import "./ServicesSlider.css";
import { Carousel } from "react-responsive-carousel";

let selected = ''

export default class ServicesSlider extends Component {
  renderImages() {
    return Object.keys(this.props.moreImages).map((img, index) => {
      const image = this.props.moreImages[img]; 
      if(this.props.selectedImage.alt === image.alt && !!this.props.selectedImage) {
        selected = img 
      }
      return (
        <div key={index} className="service-slider">
          <img src={image.src} alt={image.alt} />
        </div>
      );
    });
  }
  render() {  
    return (
      // <div>
      //   <Slider {...settings}>{this.renderImages()}</Slider>
      // </div>
      <Carousel
        selectedItem={0}
        autoPlay={true}
        emulateTouch
        centerSlidePercentage={65}
        centerMode={true}
        infiniteLoop
        width="100%"
        className="services-slider"
      >
        {this.renderImages()}
      </Carousel>
    );
  }
}
