import React from 'react';
import './BackDrop.css';

const BackDrop = props => <div className="ZoomBackDrop" onClick={props.onClick}> {props.children} </div>

export default BackDrop;