import React, { Component } from "react";
import Slider from "react-slick";
import {OrderButton} from "../UI/Button/Button";
import "./MainSlider.css";

function SampleNextArrow(props) {
  const { className, onClick } = props;
  return <div className={`${className} arrow`} onClick={onClick} />;
}

function SamplePrevArrow(props) {
  const { className, onClick } = props;
  return <div className={`${className}  arrow`} onClick={onClick} />;
}
export default class mainSlider extends Component {
  state = {
    images: [
      {
        src: require("../../img/1.jpg"),
        alt: "Hio",
        text: "При покупке тротуарной плитки более 100 м² ",
        discount: "получите 5% скидку"
      },
      {
        src: require("../../img/6.jpg"),
        alt: "Hio",
        text: "До 10км доставка",
        discount: "бесплатно"
      },
      {
        src: require("../../img/3.jpg"),
        alt: "Hio",
        text: "Художественная ковка",
        // discount: "Скидки до 15%"
      },
      {
        src: require("../../img/4.jpg"),
        alt: "Hio",
        text: "Укладка тротуарной плитки",
        // discount: "Скидки до 15%"
      },
      {
        src: require("../../img/5.jpg"),
        alt: "Hio",
        text: "Асфальтирование дорог",
        // discount: "Скидки до 15%"
      }
    ]
  };
  renderImages() {
    return Object.keys(this.state.images).map((img, index) => {
      const image = this.state.images[img];
      return (
        <div className="overlay" key={index}>
          <img src={image.src} alt={image.alt} />
          <div className="slider-text">
            <h1>{image.text}</h1>       
            {/* <h1>{image.discount}</h1>      */}
            <h1 className="discount">{image.discount ? image.discount : null } </h1>     
             <OrderButton />
          </div>
        </div>
      );
    });
  }

  render() {
    const settings = {
      infinite: true,
      speed: 1000,
      autoplay: true,
      autoplaySpeed: 8500,
      arrows: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      pauseOnFocus: false,
      pauseOnHover: false,
      adaptiveHeight: true,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
    };
    return (
      <div className="MainSlider">
        <Slider {...settings}>
          {this.renderImages()}
          
        </Slider>
      </div>
    );
  }
}
