import React, { Component } from "react";
import Input from "../UI/Input/Input";
import "./LeaveUsMessage.css";
import axios from "axios";
import moment from "moment";
import SnackbarComponent from "../UI/Snackbar/Snackbar";
// import { reqURL } from '../../config/reqURL'
import { reqURL } from "../../config/reqURL";

class LeaveUsMessage extends Component {
  constructor(props) {
    super(props);
    this._initState = {
      isSended: false,
      isFormValid: false,
      formControls: {
        name: {
          value: "",
          label: "Ваше имя*",
          type: "text",
          valid: false,
          touched: false,
          errorMessage: "Գրեք ճիշտ անուն",
          validation: {
            required: true,
            minLength: 3,
            maxLength: 32
          }
        },
        phone: {
          value: "",
          label: "Ваш телефон*",
          type: "text",
          valid: false,
          touched: false,
          errorMessage: "Գրեք ճիշտ անուն",
          validation: {
            required: true,
            minLength: 4,
            maxLength: 32
          }
        }
      },
      message: {
        value: "",
        label: "Ваши комментарии",
        type: "text",
        valid: false,
        touched: false,
        errorMessage: "Գրեք ճիշտ անուն",
        validation: {
          minLength: 4,
          maxLength: 32
        }
      }
    };

    this.state = this._initState;
  }

  validateControl(value, validation) {
    if (!validation) {
      return true;
    }

    let isValid = true;
    if (validation.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (validation.minLength) {
      isValid = value.length >= validation.minLength && isValid;
    }

    if (validation.maxLength) {
      isValid = value.length <= validation.maxLength && isValid;
    }

    return isValid;
  }

  onChangeHandler = (event, controlName) => {
    const formControls = { ...this.state.formControls };
    const control = { ...formControls[controlName] };

    control.value = event.target.value;
    control.touched = true;
    control.valid = this.validateControl(control.value, control.validation);

    formControls[controlName] = control;

    let isFormValid = true;
    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid;
    });

    this.setState({
      formControls,
      isFormValid
    });
  };

  /** textarea onchange handler */
  textareaHandler = (event, controlName) => {
    event.preventDefault();
    const message = { ...this.state.message };
    message.value = event.target.value;
    this.setState({
      message
    });
  };

  renderInputs() {
    return Object.keys(this.state.formControls).map((controlName, index) => {
      const control = this.state.formControls[controlName];
      return (
        <Input
          label={control.label}
          key={controlName + index}
          type={control.type}
          value={control.value}
          touched={control.touched}
          valid={control.valid}
          onChange={event => this.onChangeHandler(event, controlName)}
        />
      );
    });
  }

  sendMessageHandler = async event => {
    event.preventDefault();
    let today = moment().format("DD/MM/YY, h:mm:ss a");
    const data = {
      name: this.state.formControls.name.value,
      phone: this.state.formControls.phone.value,
      message: this.state.message.value,
      date: today
    };

    try {
      const response = await axios.post(reqURL.sendMessage, data);
      if (response.status === 201) {
        this.setState({ isSended: true });
        setTimeout(() => {
          this.setState(this._initState);
        }, 1000);
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <div className="LeaveUsMessage_main-wrapper">
        <h2>ЗАПИСАТЬСЯ НА КОНСУЛЬТАЦИЮ</h2>
        <div className="LeaveUsMessage_wrapper">
          <div className="LeaveUsMessage">
            <h3>
              Воспользуйтесь формой, чтобы получить бесплатную консультацию
            </h3>

            <div className="message-block">
              <div className="message-input">{this.renderInputs()}</div>
              <div className="textarea">
                <label>
                  <div>{this.state.message.label}</div>
                  <textarea
                    className="message-text"
                    value={this.state.message.value}
                    onChange={event =>
                      this.textareaHandler(event, this.state.message)
                    }
                  />
                </label>
              </div>
            </div>
            <div className="Button send-button">
              <button
                type="submit"
                onClick={this.sendMessageHandler}
                className="send-btn"
              >
                Отправить
              </button>
            </div>
            {this.state.isSended ? (
              <SnackbarComponent
                adminChange={this.state.isSended}
                open={this.state.isSended}
                message={`${
                  this.state.formControls.name.value
                } сообшение отправленно`}
              />
            ) : null}
          </div>

          <div className="work-time">
            <div
              style={{
                display: "flex",
                marginTop: "10px",
                alignItems: "center"
              }}
            >
              <i
                className="fas fa-map-marker-alt"
                style={{ marginRight: "5px" }}
              />
              <p style={{ padding: "5px", fontWeight: "bold" }}>
                Принимаем заказы по следующим районам: Воскресенск, Раменское , Домодедово.
              </p>
            </div>

            <div
              style={{
                display: "flex",
                marginTop: "10px",
                alignItems: "center"
              }}
            >
              <i className="far fa-clock" style={{ marginRight: "5px" }} />
              <p style={{ padding: "5px", fontWeight: "bold" }}>
                Работаем без выходных с 8:00 по 22:00
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default LeaveUsMessage;
