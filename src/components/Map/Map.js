import { YMaps, Map, Placemark } from "react-yandex-maps";
import React from "react";
import './Map.css'

export default function MapComponent()   {
  return (
    <div className="main-map" >
      <h2>Мы здесь</h2>
      <div className="main-map__map">
        <YMaps>
          <Map
            instanceRef={ref => {
              ref && ref.behaviors.disable("scrollZoom");
            }}
            width="100vw"
            height="400px"
            defaultState={{
              center: [55.3455160292706, 38.73697296592468],
              zoom: 12
            }}
          >
            {" "}
            <Placemark geometry={[55.3455160292706, 38.73697296592468]} />
          </Map>
        </YMaps>
      </div>
    </div>
  );
};
