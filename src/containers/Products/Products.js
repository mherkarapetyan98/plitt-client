import React, { Component } from "react";
import "./Products.css";
import { Route, Switch, withRouter } from "react-router-dom";
import Trotuar from "./content/trotuar/Trotuar";
import { NavLink } from "react-router-dom";
import ProductsMenu from "./ProductsMenu/ProductsMenu";
import AboutProducts from "./content/AboutProducts/AboutProducts";
import { reqURL } from "../../config/reqURL";
import axios from "axios"

class Products extends Component {
  state = {
    value: 0,
    categories: [],
  };


  renderLinks() {
    const { categories } = this.state;
    return categories.map((category, index) => {
      return (
        <li key={category.key}>
          <NavLink
            to={`/products/${category._id}`}
            exact={true}
            activeStyle={{ background: "rgb(230, 177, 177)" }}
          >
            {category.name}
          </NavLink>
        </li>
      );
    });
  }

  async componentDidMount() {
    document.title = "Продукты "
    try {
      const response = await axios.get(reqURL.getCategory)
      const categories = response.data;
      this.setState({
        categories
      })
      
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    const { categories } = this.state;

    return (
      <div className="Products">
        <div className="sidebar">
          <div className="products-menu">
            <div className="ProductsMenu">
              <div className="ProductsMenu__header">
                <h4>Продукты</h4>
              </div>
              <div className="ProductsMenu__content">
                <ul>
                  <li>
                    <NavLink
                      to="/products"
                      exact={true}
                      activeStyle={{ background: "rgb(230, 177, 177)" }}
                    >
                      О продуктах
                    </NavLink>
                  </li>
                  {this.renderLinks()}
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="products-content">
          <Switch>
            {categories.map(category => {
              return (
                <Route path={`/products/${category._id}`} exact key={category.key} render={(props) => <Trotuar {...props} categoryKey={category.key} categoryId={category._id} key={category.key} name={category.name}/>} />
             )
            })}
            <Route path="/products" exact component={AboutProducts} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(Products);
