import React, { Component } from "react";
import "./ProductsMenu.css";

import { NavLink } from "react-router-dom";

const links = [
  { to: "/products", label: "О продуктах", exact: true },
  { to: "/products/trotuar", label: "Тротуарная плитка", exact: true }
];

// es linkerin vonc em talu??? trotuar@ petq poxvi zut categorykey?

class ProductsMenu extends Component {

 

  renderLinks() {

    // this.state.categories.map,  links @ el chem ogtagorcum??? ba about @
    return links.map((link, index) => {
        return (
        <li key={index}>
          <NavLink
            to={link.to}
            exact={link.exact}
            activeStyle={{ background: "rgb(230, 177, 177)" }}
          >
            {link.label}
          </NavLink>
        </li>
      );
    });
  }

  render() {
    return (
      <div className="ProductsMenu">
        <div className="ProductsMenu__header">
          <h4>Продукты</h4>
        </div>
        <div className="ProductsMenu__content">
          <ul>{this.renderLinks()}</ul>
        </div>
      </div>
    );
  }
}
export default ProductsMenu;
