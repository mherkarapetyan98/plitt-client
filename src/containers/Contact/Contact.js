import React, { Component } from "react";
import "./Contact.css";
import MapComponent from "../../components/Map/Map";
import LeaveUsMessage from "../../components/leaveUsMessage/LeaveUsMessage";

class Contact extends Component {
  state = {
    isHovered: false,
    isCallHovered: false
  }
  hoverHandler = (event) =>{
    event.preventDefault();    
    
      this.setState({
        isHovered: true
      })     
  }
 
  mouseOutHandler = (event) => {
    event.preventDefault();
    this.setState({
      isHovered: false
    })
  }

  
  hoverCallHandler = (event) =>{
    event.preventDefault();    
    
      this.setState({
        isCallHovered: true
      })     
  }

  mouseOutCallHandler = (event) => {
    event.preventDefault();
    this.setState({
      isCallHovered: false
    })
  }
 
  // componentDidMount() {
  //   this.hoverHandler();
  //   this.mouseOutHandler();
  //   this.hoverCallHandler();
  //   this.mouseOutCallHandler()
  // }

  componentDidMount() {
    document.title = "Контактная информация"
  }

  render() {
    return (
      <div>
        <div className="Contact">
          <h2>Контакты</h2>
          <div className="all-contact">
            <div className="contact__phones contact-block" onMouseOver={this.hoverCallHandler} onMouseOut={this.mouseOutCallHandler}>
              <i className={`fas fa-headset ${this.state.isCallHovered ? 'animateCall' : ''}`}/>
              <div className="phones__numbers">
                <p>8-916-12-12-777</p>
                <p>8-966-105-75-20</p>
              </div>
              <div className="phones__icons">
                <div className="icons__viber">
                  <i className="fab fa-viber" style={{color: '#8f5db7'}}/>
                  <p>Viber</p>
                </div>
                <div className="icons__wp">
                  <i className="fab fa-whatsapp" style={{color: '#25d366'}}/>
                  <p>Whats App</p>
                </div>
              </div>
            </div>
            <div className="contact__addresses contact-block" onMouseOver={this.hoverHandler} onMouseOut={this.mouseOutHandler}>
              <i className={`fas fa-map-marker-alt ${this.state.isHovered ? 'animatePointer' : ''}`} />
              <p>
                улица Старая Промплощадка, 1А, Россия, Московская область,
                Воскресенск
              </p> 
              <a href="#contact-map">Посмотреть на карте</a>
            </div>
          </div>
        </div>
        <div className="main-contact">
          <div>
            <LeaveUsMessage />
          </div>
        </div>
        <div id="contact-map" >
        <MapComponent />

        </div>
      </div>
    );
  }
}

export default Contact;
