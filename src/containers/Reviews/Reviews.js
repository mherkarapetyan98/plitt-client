import "./Reviews.css";
import React, { Component } from "react";
import axios from "axios";
import moment from "moment";
import Loader from "../../components/UI/Loader/Loader";

export default class Reviews extends Component {
  state = {
    pages: "",
    page: "",
    next: "",
    prev: "",
    isAdded: false,
    reviews: [],
    name: "",
    email: "",
    review: "",
    loading: true
  };
  renderReviews = () => {
    return this.state.reviews.map(({ date, email, name, review, _id }) => {
      const dateToShow = date  ? date.match(/^.*?(?=T)/)[0] : null; 
      return (
        <div className="review-item" key={_id}>
          <blockquote>
            <b style={{ fontSize: 13 }}>{name}</b>{" "}
            <span style={{ color: "#ccc", fontSize: 11 }}>{dateToShow}</span>
            <p style={{ fontStyle: "italic" }}>{review}</p>
          </blockquote>
        </div>
      );
    });
  };

  async getAllReviews() {
    try {
      const response = await axios.get("http://plitt.ru/api/reviews");
      const reviews = response.data; 
      this.setState({
        pages: response.data.pages,
        page: response.data.page,
        next: response.data.next,
        prev: response.data.prev,
        reviews: reviews.reviews,
        loading: false
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentDidMount() {
    this.getAllReviews();
  }

  onSubmitFormHandler = async e => {
    e.preventDefault();
    // let today = moment().format("YYYY-mm-dd");
    // let today = moment([year, month-1, day, hour, minute]);
    var localTime = moment().format("YYYY-MM-DD"); // store localTime
    var proposedDate = localTime + "T00:00:00.000Z";
    var isValidDate = moment(proposedDate).isValid();
    var momentDate = moment(proposedDate); 

    const data = {
      name: this.state.name,
      email: this.state.email,
      review: this.state.review,
      date: momentDate.format("YYYY-MM-DD hh:mm:ss")
    };
    try {
      const response = await axios.post(
        "http://plitt.ru/api/reviews",
        data
      );
      this.getAllReviews();
      this.setState({
        name: "",
        email: "",
        review: ""
      });
    } catch (error) {
      console.log(error);
    }
  };

  pagesHandler = async (event, number) => {
    event.preventDefault();
    try {
      const url = `http://plitt.ru/api/reviews/?page=${number}`;
      const response = await axios.get(url);
      window.scrollTo(0, 0);
      this.setState({
        pages: response.data.pages,
        page: response.data.page,
        next: response.data.next,
        prev: response.data.prev,
        reviews: response.data.reviews,
        loading: false
      });
    } catch (error) {
      console.log(error);
    }
  };

  renderPagination = () => {
    const pages = this.state.pages;
    const numbers = [];
    for (let i = 1; i <= pages; i++) {
      numbers.push(i);
    }
    return numbers.map(number => {
      return (
        <li
          key={number}
          className={number === this.state.page ? "active_page" : ""}
          onClick={event => this.pagesHandler(event, number)}
        >
          {number}
        </li>
      );
    });
  };

  render() {
    return (
      <div className="Reviews">
        <h2>Отзывы наших клиентов</h2>
        <div className="reviews-block">
          <div className="reviews-block__item">
            {this.state.loading ? <Loader /> : this.renderReviews()}
            <div className="pagination-block">
              <ul>{this.renderPagination()}</ul>
            </div>
          </div>
          <div className="reviews-block__form">
            <h4>
              Вы можете оставить свой отзыв.
              <br />
              Для нас важно ваше мнение!
            </h4>

            <input
              placeholder="Ваше имя*"
              className="inputFields"
              value={this.state.name || ""}
              onChange={e => this.setState({ name: e.target.value })}
            />
            <input
              placeholder="E-mail*"
              className="inputFields"
              value={this.state.email || ""}
              onChange={e => this.setState({ email: e.target.value })}
            />

            <textarea
              cols="30"
              rows="10"
              className="inputFields"
              value={this.state.review}
              placeholder="Ваше сообщение"
              onChange={e => this.setState({ review: e.target.value })}
            />
            <p>Поля, отмеченные *, являются обязательными для заполнения</p>
            <button onClick={e => this.onSubmitFormHandler(e)}>
              Опубликовать отзыв
            </button>
          </div>
        </div>
      </div>
    );
  }
}
