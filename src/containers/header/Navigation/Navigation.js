import React, { Component } from "react";
import HeaderMenu from "./HeaderMenu/HeaderMenu";
import { NavLink } from "react-router-dom";
import "./Navigation.css";

class Navigation extends Component {

  render() {
    return (
      <div className="Navigation">
        <div className="nav-wrapper">
          <div className="navigation__logo">
            <NavLink to="/">
              <img src={require("../../../img/logo1.png")} alt="plitt.ru" />
            </NavLink>
          </div>
          {/* nav-menu */}
          <HeaderMenu />
        </div>
      </div>
    );
  }
}

export default Navigation;
