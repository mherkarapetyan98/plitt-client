import React, { Component } from "react";
import Navigation from './Navigation/Navigation';
import "./Header.css";

class Header extends Component {
  state = {
     isScrolled: false
  }
  handleScroll() {
    window.addEventListener("scroll", event => {
      event.preventDefault();
      event.stopPropagation();
      let scrolled = window.pageYOffset || document.documentElement.scrollTop; 
      if (scrolled > 100) {
         this.setState({isScrolled: true}) 
       } else {
        this.setState( {
          isScrolled: false
      })
      }
    });
  }
  
  componentDidMount() {
    this.handleScroll()    
  }

  render() {
    return (
      <div className={`Header ${ this.state.isScrolled ? 'header-fixed' : ''}`}>
      {/* begin top header */}
        <div className="top-header">
          <div className="top-header__contact">
            <div className="contact-address"><a href="#map">ул. Старая Промплощадка, 1А Воскресенск</a></div>
            <div className="contact-mail"><a href="mailto:plitt.ru@yandex.ru">plitt.ru@yandex.ru</a></div>
            <div className="contact-phone"><a href="tel:+79161212777">8-916-12-12-777</a></div>            
          </div> 
        </div>
       {/* end top header */}

       {/* begin navigation */}

        <Navigation />

       {/* end  navigation */}
      </div>
    );
  }
}

export default Header;
