import React from "react";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import Lightbox from "react-image-lightbox";
import "./Works.css";

class Works extends React.Component {
  state = {
    photoIndex: 0,
    isOpen: false,
    images: [
      "https://besplatka.ua/aws/37/34/89/37/plitka-trotuarnaya--professionalnaya-ukladka-photo-6c47.jpg",
      "http://fem.kiev.ua/blog/wp-content/uploads/2017/07/%D0%A3%D0%BA%D0%BB%D0%B0%D0%B4%D0%BA%D0%B0-%D1%82%D1%80%D0%BE%D1%82%D1%83%D0%B0%D1%80%D0%BD%D0%BE%D0%B9-%D0%BF%D0%BB%D0%B8%D1%82%D0%BA%D0%B8-%D0%BD%D0%B0-%D1%82%D0%B5%D1%80%D1%80%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D0%B8-%D0%A5%D1%80%D0%B0%D0%BC%D0%B0-1600x1200.jpg",
      "http://plita.kh.ua/HaIIIu/DSC08552.jpg",
      "https://www.remontbp.com/wp-content/uploads/2013/07/Kladka-trotuarnoj-plitki.jpeg",
      "https://images.ru.prom.st/261142910_w640_h640_bruschatka-trotuarnaya-.jpg",
      "https://innstroy.ru/sites/default/files/news/ukladka-trotuarnoy-plitki-5.png",
      "https://flint-stone.ru/images/phocagallery/dlya-stranic/thumbs/phoca_thumb_l_01.jpg",
      "https://csk-remont.ru/wp-content/uploads/2016/02/6-7.jpg",
      "https://www.mosobltrotuar.ru/upload/medialibrary/d22/d22dba98b7ad853bebdf32e834c1a3ac.jpg",
    ]
  };

  renderImages = () => {
    let photoIndex = -1;
    const { images } = this.state;

    return images.map(imageSrc => {
      photoIndex++;
      const privateKey = photoIndex;
      return (
        <img
          src={imageSrc}
          alt="Gallery"
          className="img-fluid gallery__image"
          onClick={() =>
            this.setState({ photoIndex: privateKey, isOpen: true })
          }
        />
      );
    });
  };

  render() {
    const { photoIndex, isOpen, images } = this.state;
    return (
      <div className="Works">
        <h2>Наши работы</h2>
        <div className="gallery">{this.renderImages()}</div>
        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            imageTitle={photoIndex + 1 + "/" + images.length}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length
              })
            }
          />
        )}
      </div>
    );
  }
}

export default Works;
