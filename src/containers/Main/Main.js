import React, { Component } from "react";
import "./Main.css";

import MapComponent from "../../components/Map/Map";
import MainSlider from "../../components/Slider/MainSlider";
import { ServicesButton } from "../../components/UI/Button/Button";
import LeaveUsMessage from "../../components/leaveUsMessage/LeaveUsMessage";
// import { Object } from "core-js";

class Main extends Component {
  state = {
    isScrolled: false,
    mainServices: [
      {
        src: require("../../img/icons/services/draw.png"),
        text:
          "С нашей стороны проводится изучение местности и обмерные работы",
        alt: "draw"
      },
      {
        src: require("../../img/icons/services/work.png"),
        text:
          "Мы делаем различные строительные работы",
        alt: "work"
      },
      {
        src: require("../../img/icons/services/ship.png"),
        text:
          "Быстрая доставка по указанному вами адресу",
        alt: "ship"
      }
    ]
  };

  handleScroll() {
    window.addEventListener("scroll", event => {
      let scrolled = window.pageYOffset || document.documentElement.scrollTop;
      if (scrolled > 100) {
        this.setState({ isScrolled: true });
      } else {
        this.setState({
          isScrolled: false
        });
      }
    });
  }
  componentDidMount() {
    this.handleScroll();
  }

  renderMainServices() {
    return Object.keys(this.state.mainServices).map(index => {
      const service = this.state.mainServices[index];
      return (
        <div
          className={`main-services__service ${
            this.state.isScrolled ? "show-hided" : ""
          }`}
          key={index}
        >
          <img src={service.src} alt={service.alt} />
          <p>{service.text}</p>
        </div>
      );
    });
  }

  render() {
    return (
      <div className="Main">
        <div>
          <MainSlider />
        </div>
        {/*  TODO: begin services */}
        <div className="main-services">
          <h2 className={this.state.isScrolled ? "show-hided" : ""}>Услуги</h2>
          <div className="service-content">{this.renderMainServices()}</div>
          <div
            className={`services__orderButton ${
              this.state.isScrolled ? "show-hided" : ""
            }`}
          >
            {/* TODO: change link to services  */}
            <ServicesButton />
          </div>
        </div>
        {/*   end services */}

        {/* begin contact */}
        <div className="main-contact">
          
          <div>
            <LeaveUsMessage />
          </div>
        </div>
        {/* end contact */}

        {/* begin map */}
        <div id="map">
            <MapComponent/>
        </div>

        {/* end map */}
      </div>
    );
  }
}

export default Main;
