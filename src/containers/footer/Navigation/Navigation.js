import React, { Component } from "react";
import FooterMenu from "./FooterMenu/FooterMenu";
import "./Navigation.css";

class Navigation extends Component {
  render() {
    return (
      <div className="Footer-Navigation">
        <div className="footer__nav-wrapper">
          {/* nav-menu */}
          <FooterMenu />        
               
        </div>
      </div>
    );
  }
}

export default Navigation;