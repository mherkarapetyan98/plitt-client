import React, { Component } from "react";
import Navigation from "./Navigation/Navigation";
import "./Footer.css";

class Footer extends Component {
  state = {
    isScrolled: false
  };

  render() {
    return (
      <div className={`Footer`}>
        <div className="footer-wrapper">
          <div className="footer__contact-info">
            <div className="contact-address">
              <a href="#map">ул. Старая Промплощадка, 1А Воскресенск</a>
            </div>
            <div className="contact-mail">
              <a href="mailto:plitt.ru@yandex.ru">plitt.ru@yandex.ru</a>
            </div>
            <div className="contact-phone">
              <a href="tel:+79161212777">8-916-12-12-777</a>
            </div>
          </div>
          <Navigation />
        </div>

        <div className="copyright">
          <p> &copy; 2019 Plitt.ru </p>
        </div>
      </div>
    );
  }
}

export default Footer;
